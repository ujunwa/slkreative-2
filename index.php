<?php include('header.php'); ?>

	<!-- Document Title
	============================================= -->
	<title>Home | SL Kreativez</title>


<?php include('index-nav.php'); ?>


	<!-- Page Title
		============================================= -->
		<section>
				<img src="images/sl.jpg" style="margin-top:-15px;">	

		</section><!-- #page-title end -->


		<!-- Content
		============================================= -->
		<section id="content">

			<div class="content-wrap">

				<div class="section header-stick">
					<div class="container clearfix">
						<div class="row">

							<div class="col-md-9">
								<div class="heading-block bottommargin-sm">
									<h3>We Energize Creative Artistry</h3>
								</div>

								<p class="nobottommargin">Our field of interest cuts across Storytelling, Articles and Research Documentary, Nollywood and Stage Production Reviews, African Literature Reviews, Exclusive Industry Interviews, Mini-drama Productions, Creative Writing Projects, Conferences/Seminars and Art Workshops, Literacy/charity Works</p>
							</div>

							<div class="col-md-3"  >
								<a href="services.php" style="background-color:#0087BD;" class="button button-3d button-dark button-large btn-block center" style="margin-top: 5px;">What We Do</a>
							</div>

						</div>
					</div>
				</div>

				<div class="container clearfix">

					<div class="col_one_third">
						<div class="feature-box media-box">
							<div class="fbox-media">
								<img src="images/us.jpg" alt="Who We Are" >
							</div>
							<div class="fbox-desc">
								<div class="heading-block fancy-title nobottomborder title-bottom-border">
									<h4>Who We <span>ARE</span>.</h4>
								</div>
								<p>We're a social enterprise with interest in developing the next generation of writers and creative geniuses. Our interest is to create tools, opportunities and platforms to experiment, practice and create works of art worth celebrating and emulating. Our field of interest cuts across Storytelling, Articles and Research Documentary...</p>
								<br>
								<a href="about.php" class="button button-3d noleftmargin">Read More</a>
							</div>
						</div>
					</div>

					<div class="col_one_third">
						<div class="feature-box media-box">
							<div class="fbox-media" >
								<img src="images/mission.jpg" alt="Our Mission">
							</div>
							<div class="fbox-desc" >
								<div class="heading-block fancy-title nobottomborder title-bottom-border" style="padding-top:4px;">
									<h4>Core <span>Mission</span>.</h4>
								</div>

								<p>We want to be the energy behind the rejuvenation of the dying theatre and literary arts culture among young practitioners, graduates and undergraduates of tertiary institutions.We want to be at the forefront of awareness and promotion of literary culture in secondary school students and beyond. Our mission is summed up in these three point focus...
								</p><br>
								<a href="about.php" class="button button-3d noleftmargin">Read More</a>
							</div>
						</div>
					</div>

					<div class="col_one_third col_last">
						<div class="feature-box media-box">
							<div class="fbox-media">
								<video preload="auto" loop autoplay muted width="400px" height="215px" >
									<source src='images/videos/deskwork.mp4' type='video/mp4' />
								</video>
							</div>
							
							<div class="fbox-desc" >
								<div class="heading-block fancy-title nobottomborder title-bottom-border" style="padding-top:4px;">
									<h4>Our <span>Story</span>.</h4>
								</div>

								<p>SL Kreativez is an offspring of the restless spirit of a Nigerian girl, by name, Esther Okoloeze. She started scribbling right from secondary school and with exposure to acting through her teenage church; she went on to study Theatre Arts from the Prestigious University of Benin where she graduated among the best of her class..
								</p><br>
								<a href="about.php" class="button button-3d noleftmargin">Read More</a>
							</div>
						</div>
					</div>

					<div class="clear"></div>


					

					<div class="col_one_third">
						<div class="feature-box media-box">
							<div class="fbox-media" >
								<img src="images/try.jpg" alt="Our Name">
							</div>
							<div class="fbox-desc" >
								<div class="heading-block fancy-title nobottomborder title-bottom-border" style="padding-top:4px;">
									<h4>Our <span>Name</span>.</h4>
								</div>

								<p>SL Kreativez stands for Seekers Locus Kreativez. Seekers as a name implies people in search of a meaning, a voice and an adventure. Locus is where this can be found. Kreativez is a hype version of  Creative.In essence, we say, seekers locus is the place where creativity finds relevance, a voice and a sense of adventure.
								</p><br>
								<a href="about.php" class="button button-3d noleftmargin">Read More</a>
							</div>
						</div>
					</div>

					<div class="col_one_third">
						<div class="feature-box media-box">
							<div class="fbox-media">
								<img src="images/vision.jpg" alt="Our Vision" >
							</div>
							<div class="fbox-desc">
								<div class="heading-block fancy-title nobottomborder title-bottom-border">
									<h4>Our <span>Vision</span>.</h4>
								</div>
								<p>Seekers Locus Kreativez (SLK) is a social enterprise that is based on the vision of Esther Okoloeze to create a platform where creativity in its true sense would not only thrive but the creators get exposed to resources, opportunities and get rewarded for their intellects and skills. We want have a culture that appreciates...</p>
								<br>
								<a href="about.php" class="button button-3d noleftmargin">Read More</a>
							</div>
						</div>
					</div>

					<div class="col_one_third col_last">
						<div class="feature-box media-box">
							<div class="fbox-media">
								<img src="images/path.jpg" alt="Our Vision">
							</div>
							<div class="fbox-desc" >
								<div class="heading-block fancy-title nobottomborder title-bottom-border" style="padding-top:4px;">
									<h4>How will <span>We Get there</span>.</h4>
								</div>

								<p>By creating a platform that energizes the creative spirit of every creative seeker. Promoting their creativity, one person at a time, we will lend a voice to the struggle and a hand to the hustle. This we will do by supporting, educating, training and promoting the young ones. These are our blueprint for now and the future
								</p><br>
								<a href="about.php" class="button button-3d noleftmargin">Read More</a>
							</div>
						</div>
					</div>

					<div class="clear"></div>
				</div>

				<div class="section nomargin" style="padding-top: 80px;">
					<div class="heading-block center">
						<h2>The Team Players</h2>
					</div>
				
					<div class="container">
						<div id="oc-team-list" class="container owl-carousel team-carousel carousel-widget" data-margin="30" data-nav="false" data-items-sm="1" data-items-lg="1" style="padding-bottom: 40px;">

						
							<div class="oc-item">
								<div class="team team-list clearfix">
									<div class="team-image">
										<img src="images/esther.jpg" alt="Esther Okoloeze">
									</div>
									<div class="team-desc">
										<div class="team-title"><h4>Esther Okoloeze</h4><span>Founder, Leader, Writer and Multitasking Learner</span></div>
										<div class="team-content">
											<p>Esther with the stuck nickname HRM Queen Esther is a Second Class Upper graduate of Theatre Arts from the University of Benin, Benin City. She is currently a distant learning Masters student in Media and Mass Communication Management. she speaks English, Igbo, Yoruba and Nigerian Pidgin fluently and Her interest spans through Film, Art, Literature, Creative writing for stage and screen, Communication, Language and Entrepreneurship.</p>
										</div>
										<br>
										<ul style="text-decoration:none; margin: 0 auto; ">
											<li style="display:inline-block;">
												<a href="https://www.facebook.com/Queen.Esther.Okoloeze" target="_blank"><img src="images/fb.png"></a>
											</li>
											<li style="display:inline-block;">
												<a href="https://twitter.com/esther_okoloeze" target="_blank"><img src="images/tw.png"></a>
											</li>
											<li style="display:inline-block;">
												<a href="https://www.instagram.com/kween_estta/" target="_blank"><img src="images/insta.png"></a>
											</li>
											<li style="display:inline-block;">
												<a href="https://www.linkedin.com/in/esther-okoloeze-15a3b8105" target="_blank"><img src="images/link.png"></a>
											</li>
										</ul>	
										
									</div>
								</div>
							</div>
							<div class="oc-item">
								<div class="team team-list">
									<div class="team-image">
										<img src="images/ebere.jpg" alt="Ebere Akuche Vivian">
									</div>
									<div class="team-desc">
										<div class="team-title"><h4>Ebere Akuche Vivian</h4><span>Creative Editor and Literature Review Analyst</span></div>
										<div class="team-content">
											<p>Vivian Akuche also known as ExplicitEva is an Anambra born, Lagos bred. She is an undergraduate of Mass communication from Federal University of Technology, Akure (FUTO).Her interests are in History, Literature, Music and Art. She boasts of finesse skill in writing and graphics design. She enjoys Reading, Travelling and Listening to music.She aspires to be a Counselor, a journalist and an Activist in the not so far future.</p>
										</div>
										<br>
										<ul style="text-decoration:none; margin: 0 auto; ">
											<li style="display:inline-block;">
												<a href="https://www.facebook.com/vivianakuche" target="_blank"><img src="images/fb.png"></a>
											</li>
											<li style="display:inline-block;">
												<a href="https://twitter.com/evakuche" target="_blank"><img src="images/tw.png"></a>
											</li>
											<li style="display:inline-block;">
												<a href="https://www.instagram.com/evakuche/" target="_blank"><img src="images/insta.png"></a>
											</li>
											<li style="display:inline-block;">
												<a href="https://www.linkedin.com/in/ebere-akuche-b8a48114a" target="_blank"><img src="images/link.png"></a>
											</li>


										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- Page Title
				============================================= -->
				<section>

					<a href="jobs.php"><img src="images/hiring.jpg"></a>

				</section><!-- #page-title end -->
				

				<div style="padding-top: 80px;">

					<div class="heading-block center">
						<h2>Our Services.</h2>
						<span>Our field of interest cuts across</span>
					</div>

					<div class="container">

						<div class="col-md-6 col-sm-6 topmargin bottommargin">

							<div class="col_full">

								<div class="feature-box fbox-plain fbox-small fbox-dark">
									<div class="fbox-icon">
										<a href="#"><img src="images/ibook.jpg"></a>
									</div>
									<h3>Storytelling, Articles and Research Documentary</h3>
									<p>With the best of sultry, soulful and creatively crafted fiction stories, we promise to keep you informed, entertained and educated</p>
								</div>
							</div>

							<div class="col_full">

								<div class="feature-box fbox-plain fbox-small fbox-dark">
									<div class="fbox-icon">
										<a href="#"><img src="images/imov.jpg"></a>
									</div>
									<h3>Nollywood and Stage Production Reviews</h3>
									<p>This is your number one stop for professional reviews and critique of Nollywood films and stage arts</p>
								</div>
							</div>

							<div class="col_full">

								<div class="feature-box fbox-plain fbox-small fbox-dark">
									<div class="fbox-icon">
										<a href="#"><img src="images/imic.jpg"></a>
									</div>
									<h3>Exclusive Industry Interviews</h3>
									<p>Who wouldn't love to celebrate those who makes our heart dance with words well crafted? We would!</p>
								</div>
							</div>

							<div class="col_full nobottommargin">

								<div class="feature-box fbox-plain fbox-small fbox-dark">
									<div class="fbox-icon">
										<a href="#"><img src="images/irev.jpg"></a>
									</div>
									<h3>African Literature Reviews</h3>
									<p>We want to have an archive of literature reviews that can help students, professionals and literature lovers. </p>
								</div>
							</div>

						</div>

						<div class="col-md-6 col-sm-6 topmargin bottommargin">

							<div class="col_full">

								<div class="feature-box fbox-plain fbox-small fbox-dark">
									<div class="fbox-icon">
										<a href="#"><img src="images/iconf.jpg"></a>
									</div>
									<h3>Conferences/Seminars and Art Workshops</h3>
									<p>Learn! Learn! Learn! We have to keep learning, networking, developing till we become like diamonds. </p>
								</div>
							</div>

							<div class="col_full">

								<div class="feature-box fbox-plain fbox-small fbox-dark">
									<div class="fbox-icon">
										<a href="#"><img src="images/ipaint.jpg"></a>
									</div>
									<h3>Literacy/charity Works</h3>
									<p>Literacy is the bedrock of a forward thinking nation. We are open for partnership and volunteering on any project.</p>
								</div>
							</div>

							<div class="col_full">

								<div class="feature-box fbox-plain fbox-small fbox-dark">
									<div class="fbox-icon">
										<a href="#"><img src="images/ipen.jpg"></a>
									</div>
									<h3>Creative Writing Projects</h3>
									<p>Yes, remember we're a for-profit social enterprise. This is where we channel our social goals.</p>
								</div>
							</div>

							<div class="col_full nobottommargin">

								<div class="feature-box fbox-plain fbox-small fbox-dark">
									<div class="fbox-icon">
										<a href="#"><img src="images/idrama.jpg"></a>
									</div>
									<h3>Mini-drama Productions</h3>
									<p>You can watch videos on different segments of what we do. This is in the Loading!.</p>
								</div>
							</div>

						</div>

					</div>

				</div>

				<div class="promo promo-dark promo-flat promo-full footer-stick"  style="background-color:#0087BD;">
					<div class="container clearfix text-center">
						<h3 style="font-size: 18px;">Call us today at <span>08012345678</span> or Email us at <span>info@seekerslocus.com</span></h3>
					</div>
				</div>

			</div>

		</section><!-- #content end -->

		

		<!-- Footer
		============================================= -->
<?php include('footer.php'); ?>